package com.jk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootProvierApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootProvierApplication.class, args);
    }

}

